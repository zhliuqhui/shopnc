<?php
/**
 * 默认展示页面
 * 
 * 二次开发联系：76809326 	加好友备注(ShopNCO2O二次开发)
 *
 * by 运维舫 www.shopnc.club
 */
defined('InShopNC') or exit('Access Invalid!');

class indexControl extends SystemControl{
	public function __construct(){
		parent::__construct();
		Language::read('indexs');
	}
	public function indexOp(){
		//输出管理员信息
		Tpl::output('admin_info',$this->getAdminInfo());
		Tpl::showpage('index','index_layout');
	}

	/**
	 * 退出
	 */
	public function logoutOp(){
		session_unset();
		session_destroy();
		setNcCookie('sys_key','',-1,'',null);
		@header("Location: index.php");
		exit;
	}

	

}
