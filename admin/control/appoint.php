<?php
/**
 * 站点设置
 * 
 * 二次开发联系：76809326 	加好友备注(ShopNCO2O二次开发)
 *
 * by 运维舫 www.shopnc.club
 */
defined('InShopNC') or exit('Access Invalid!');

class appointControl extends SystemControl{
	public function __construct(){
		parent::__construct();
		Language::read('store');
	}

	/**
	 * 评论列表
	 */
	public function appointlistOp(){
		$appoint_model = Model('appoint');
		$condition	 = array();
		$list = $appoint_model->getList($condition);
		Tpl::output('list',$list);
		Tpl::output('show_page',$appoint_model->showpage(2));
		Tpl::showpage('appoint.list');
	}

	public function detailOp(){
		
	}
}
