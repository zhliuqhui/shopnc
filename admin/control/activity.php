<?php
/**
 * 活动管理
 * 
 * 二次开发联系：76809326 	加好友备注(ShopNCO2O二次开发)
 *
 * by 运维舫 www.shopnc.club
 */
defined('InShopNC') or exit('Access Invalid!');

class activityControl extends SystemControl{
	public function __construct(){
		parent::__construct();
		Language::read('website');
	}
	
	/*
	 * 活动列表
	 */
	public function activityOp(){	
		
		$model = Model();
		$activity = $model->table('activity')->select();	
		Tpl::output('list',$activity);
		
		//分页
		Tpl::output('show_page',$model->showpage());
		Tpl::showpage('activity.list');
	}
	
	
	/*
	 * 添加活动
	 */
	public function delactivityOp(){
		$activity_id	= intval($_GET['activity_id']);
		$model = Model();
		$result = $model->table('activity')->where(array('activity_id'=>$activity_id))->delete();
		
		//删除操作
		if($result){
			$this->showTip(L('nc_admin_website_activity_delete_succ'),'','succ');
		}else{
			$this->showTip(L('nc_admin_website_activity_delete_fail'),'','error');
		}
	}

}