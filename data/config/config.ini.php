<?php
defined('InShopNC') or exit('Access Invalid!');
$config = array();
$config['shop_site_url'] 		= 'http://localhost/shopnc/shop';
$config['cms_site_url'] 		= 'http://localhost/shopnc';
$config['microshop_site_url'] 	= 'http://localhost/shopnc/microshop';
$config['circle_site_url'] 		= 'http://localhost/shopnc/circle';
$config['admin_site_url'] 		= 'http://localhost/shopnc/admin';
$config['upload_site_url']		= 'http://localhost/shopnc/data/upload';
$config['resource_site_url']	= 'http://localhost/shopnc/data/resource';
$config['version'] 		= '2014627';
$config['setup_date'] 	= '2016-06-14 15:33:19';
$config['gip'] 			= 0;
$config['dbdriver'] 	= 'mysqli';
$config['tablepre']		= 'shopnc_';
$config['db'][1]['dbhost']  	= 'localhost';
$config['db'][1]['dbport']		= '3306';
$config['db'][1]['dbuser']  	= 'root';
$config['db'][1]['dbpwd'] 	 	= '';
$config['db'][1]['dbname']  	= 'shopnc';
$config['db'][1]['dbcharset']   = 'UTF-8';
$config['db']['slave'] 		= array();
$config['session_expire'] 	= 3600;
$config['lang_type'] 		= 'zh_cn';
$config['cookie_pre'] 		= '054D_';
$config['tpl_name'] 		= 'default';
$config['thumb']['save_type'] 		= 1;
$config['thumb']['cut_type'] = 'gd';
$config['thumb']['impath'] = '';
$config['cache']['type'] 			= 'file';
$config['memcache']['prefix']      	= '';
$config['memcache'][1]['port']     	= 11211;
$config['memcache'][1]['host']     	= '127.0.0.1';
$config['memcache'][1]['pconnect'] 	= 0;
$config['debug'] 			= true;
$config['spec_model']		= '0';
$config['payment'] 			= 1;
$config['product_indate'] 	= 7;
$config['syn_site'][] = 'shop';
$config['syn_site'][] = 'cms';
$config['syn_site'][] = 'microshop';
$config['syn_site'][] = 'circle';
$config['app_key']['shop'] = '2f1e5e7f50ad22070ab8dc4047a3be81';
$config['app_key']['cms'] = '4997bd8ff69b3c6a04a1d561249d2ea1';
$config['app_key']['microshop'] = '9e234ab82ca5cd4de6bba5d1b647261e';
$config['app_key']['circle'] = '37caada5053d72b11c7309faffaf467d';