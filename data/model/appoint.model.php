<?php
/**
 * 
 * 二次开发联系：76809326 	加好友备注(ShopNCO2O二次开发)
 *
 * by 运维舫 www.shopnc.club
 */
defined('InShopNC') or exit('Access Invalid!');
class appointModel extends Model{

    public function __construct(){
        parent::__construct('appoint');
    }

	/**
	 * 读取列表 
	 * @param array $condition
	 *
	 */
	public function getList($condition,$page=null,$order='',$field='*'){
        $result = $this->field($field)->where($condition)->page($page)->order($order)->select();
        return $result;
	}

	/**
	 * 读取列表 
	 * @param array $condition
	 *
	 */
	public function getMemberList($condition,$page=null,$order='',$field='*'){
		$on = 'comment.member_id=member.member_id';
        $result = $this->table('comment,member')->field($field)->join('left')->on($on)->where($condition)->page($page)->order($order)->select();
        return $result;
	}

    /**
	 * 读取单条记录
	 * @param array $condition
	 *
	 */
    public function getOne($condition,$order=''){

        $result = $this->where($condition)->order($order)->find();
        return $result;

    }

	/*
	 *  判断是否存在 
	 *  @param array $condition
     *
	 */
	public function isExist($condition) {

        $result = $this->getOne($condition);
        if(empty($result)) {
            return FALSE;
        }
        else {
            return TRUE;
        }
	}

	/*
	 * 增加 
	 * @param array $param
	 * @return bool
	 */
    public function save($param){

        return $this->insert($param);	

    }
	
	/*
	 * 更新
	 * @param array $update
	 * @param array $condition
	 * @return bool
	 */
    public function modify($update, $condition){

        return $this->where($condition)->update($update);

    }
	
	/*
	 * 删除
	 * @param array $condition
	 * @return bool
	 */
    public function drop($condition){

        return $this->where($condition)->delete();

    }
	
}

