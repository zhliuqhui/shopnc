<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title><?php echo $html_title;?></title>
<link href="css/install.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="../data/resource/js/jquery.js"></script>
<script type="text/javascript" src="../data/resource/js/perfect-scrollbar.min.js"></script>
<script type="text/javascript" src="../data/resource/js/jquery.mousewheel.js"></script>
</head>

<body>
<?php defined('InShopNC') or exit('Access Invalid!');?>
<?php echo $html_header;?>
<div class="main">
  <div class="text-box" id="text-box">
    <div class="license">
      <h1>ShopNC本地生活系统安装协议</h1>
      <p class="p_center">感谢你选择运维舫电商系统。本系统由运维舫根据网域天创版本所改进版本！只用于学习交流使用。</p><br/><br/><br/><br/><br/><br/><br/>
    </div>
  </div>
  <div class="btn-box"><a href="index.php?step=1" class="btn btn-primary">同意协议进入安装</a><a href="javascript:window.close()" class="btn">不同意</a></div>
</div>
<div class="footer">
  <h5>Powered by <font class="blue">>运维</font><font class="orange">舫</font></h5>
  <h6>&copy; 2007-2014 <a href="http://www.shopnc.club" title="天津市网城创想科技有限责任公司" target="_blank">Tianjin Netcity Networking Inc.</a> All Rights Reserved.</h6>
  <h6>Software copyright registration number: 2008SR07843</h6>
</div>
<script type="text/javascript">
$(document).ready(function(){
	//自定义滚定条
	$('#text-box').perfectScrollbar();
});
</script>
</body>
</html>
